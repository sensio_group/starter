# Sensio Network Starter

This repo contains the Node under `node` and Explorer under `explorer` directories. It aims to make the development and running really easy.

Online version is available here http://explorer.sensio.network/

## Table of Contents

# Install

This project uses [node](http://nodejs.org), [yarn](https://yarnpkg.com/getting-started/install) and [rust](https://rustup.rs/). Go check them out if you don't have them locally installed.

## Locally

Clone this repo

```sh
git clone --recurse-submodules https://gitlab.com/sensio_group/starter.git
```

### Running the Node

The node is located in the `node` folder.

```sh
# enter the directory
cd node

# get the latest code
git pull origin master

# install rust
curl https://sh.rustup.rs -sSf | sh

# run init script
chmod +x ./scripts/init.sh && ./scripts/init.sh

# test the project
cargo test

# build the project
cargo build --release

# purge dev chain with auto accept
./target/release/sensio purge-chain -y

# start the chain in dev mode without telemetry
./target/release/sensio --dev --no-telemetry
```

### Running the Explorer

The explorer is located in the `explorer` folder.

```sh
# enter the directory
cd explorer

# get the latest code
git pull origin master

# install deps
yarn install

# start the explorer
yarn start
```

## With docker

Easiest and the recommended way is to use the docker-compose:

```sh
docker-compose up -d
```

This wil download the images for both node and explorer for the local testing.

The explorer will be available here: http://localhost:8111

The blockchain will be accessible `ws://localhost:9944` use this as the local node in https://polkadot.js.org/apps/

The Port values can be changes in the `docker-compose.yml` file.

The blockchain node is has telemetry disabled and it runs int the DEV mode.
